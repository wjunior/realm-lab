// import Config from 'react-native-config'
import Reactotron from 'reactotron-react-native'

const nativelog = console.log
const newLogger = (...args: any[]) => {
  nativelog(...args)

  Reactotron.display({
    name: 'CONSOLE.LOG',
    important: true,
    value: args,
    preview: args.length ? JSON.stringify(args) : args[0],
  })
}

console.log = newLogger
declare global {
  interface Console {
    tron: any
  }
}
const tron = Reactotron.useReactNative()

// if (Config.ENV !== 'PRD') {
tron.configure({ name: 'Realm db experiment', host: 'localhost' }).connect()
// }
global.tron = tron
console.tron = tron
