import { Schema } from 'db/realm-types'
import { ITaskSchema } from './types'

const TaskSchema: Schema<ITaskSchema> = {
  name: 'Task',
  primaryKey: '_id',
  properties: {
    _id: 'objectId',
    title: 'string',
    dueDate: 'date',
    priority: 'int',
    status: 'string',
    owner: 'objectId',
    progressMinutes: 'int?'
  }
}

export default TaskSchema
