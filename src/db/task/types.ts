import { Nullable } from 'db/realm-types'
import { BSON } from 'realm'

export interface ITaskSchema {
  _id: BSON.ObjectId
  title: string
  priority: number
  owner: BSON.ObjectId
  // dueDate: Nullable<Date>
  dueDate: Date
  status: string
  progressMinutes?: Nullable<number>
}

export interface ITaskInput {
  _id?: BSON.ObjectId
  title?: string
  priority?: number
  owner?: BSON.ObjectId
  dueDate?: Date
  status?: string
  progressMinutes?: number
}

export interface ITaskModel {
  create: (data: TaskType) => void
  update: (id: BSON.ObjectId, data: ITaskInput) => void
  delete: (id: BSON.ObjectId) => void
  findAll: () => Realm.Results<Realm.Object<ITaskSchema, never>>
  findById: (id: BSON.ObjectId) => Realm.Object<ITaskSchema, never>
}

export type TaskFields =
  | '_id'
  | 'title'
  | 'owner'
  | 'dueDate'
  | 'priority'
  | 'status'
  | 'progressMinutes'

export type TaskType = Realm.Object<ITaskSchema, TaskFields>
