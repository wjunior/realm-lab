import Realm from 'realm'
import { SecurityStore } from 'services'
import makeModel from './makeModel'
import schema from './schema'
import {
  ITaskInput,
  ITaskModel,
  IUserInput,
  IUserModel,
  TaskType,
  UserType,
} from './types'

const initDb = async (): Promise<IDB> => {
  try {
    const key = await SecurityStore.genCryptKey()
    const instance = await Realm.open({
      path: 'LabRealm',
      schema,
      encryptionKey: key,
      schemaVersion: 4,
    })

    const Task = makeModel<TaskType, ITaskInput>(instance, 'Task')
    const User = makeModel<UserType, IUserInput>(instance, 'User')
    return {
      Task,
      User,
    }
  } catch (error: any) {
    console.log('db connection error ====>', error.message)
    return {} as IDB
    // return new Error(error.message)
    // throw new Error({ message: error.message } as IDB)
  }
}

export interface IDB {
  User: IUserModel
  Task: ITaskModel
}

export default initDb
