import Realm from 'realm'

export type MODELS = 'User' | 'Task'

export function makeModel<S extends Partial<OmittedRealmTypes<S>>, I>(realm: Realm, model: MODELS) {
  
  return {
    findById: (id: Realm.BSON.ObjectId): S => {
      return realm.objectForPrimaryKey(model, id)
    },
    findOne: (params: string): S => {
      return realm.objects<S>(model)[0]
    },
    findAll: () => {
      return realm.objects<S>(model)
    },
    create: (params: S): S => {
      let item;
      realm.write(() => {
        item = realm.create<S>(model, params)
      })
      return item
    },
    update: (id: Realm.BSON.ObjectId, params: I): S => {
      let newItem = realm.objectForPrimaryKey<S>(model, id) ?? ({} as S)
      realm.write(() => {
        Object.keys(params).forEach((key: string) => {
          newItem[key] = params[key]
        })
      })
      return newItem
    },
    upsert: (_id: Realm.BSON.ObjectId, params: S) => {
      let item
      realm.write(() => {
        item = { _id, ...params }
        realm.create<S>(model, item, Realm.UpdateMode.Modified)
      })
      return item
    },
    delete: (_id: Realm.BSON.ObjectId): void => {
      let item
      realm.write(() => {
        item = realm.objectForPrimaryKey(model, _id)
        realm.delete(item)
        // realm.delete(model)
        item = null
      })
      // return item
    },
    clear: (): void => {
      realm.write(() => {
        realm.delete(realm.objects(model))
      })
    },
  }
}

export default makeModel
