import { RealmSchema } from './realm-schema.types'
import { Nullable } from './generic.types'

export type EntityName = 'Task' | 'User' | 'Customer'
export type EntityNameOpt = 'Task?' | 'User?' | 'Customer?'

export interface Schema<TEntity extends object>
  extends RealmSchema<TEntity, EntityName, EntityName | EntityNameOpt> {}
