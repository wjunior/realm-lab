const UserSchema = {
  name: 'User',
  properties: {
    _id: 'objectId',
    name: 'string',
  },
  primaryKey: '_id',
}

export default UserSchema
