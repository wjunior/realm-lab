import { BSON } from 'realm'

export interface IUserSchema {
  _id: 'int'
  name: 'string'
}

export interface IUserInput {
  name: 'string?'
}

export interface IUserModel {
  create: (data: UserType) => void
  update: (id: BSON.ObjectId, data: IUserInput) => void
  findAll: () => Realm.Results<Realm.Object<IUserSchema, never>>
  findById: (id: BSON.ObjectId) => Realm.Object<IUserSchema, never>
}

export type UserType = Realm.Object<IUserSchema, '_id' | 'name'>
