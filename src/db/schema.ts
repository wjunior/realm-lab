import { ObjectSchema } from 'realm'
import TaskSchema from './task'
import UserSchema from './user'

export default [TaskSchema, UserSchema] as ObjectSchema[]
