export const dark = {
  text: '#fff',
  // background: '#181a1b',
  background: 'linear-gradient(330deg, #7e40c5, #4565da)',
  colors: {
    success: '#038857',
    warn: '#ffd548',
    // danger: '#9c2a00',
    danger: 'rgb(255, 59, 48)',
    skyie: '#22bad9',
    white: '#e8e6e3',
    notification: 'rgb(255, 59, 48)',
    shadowBlue: 'rgba(0, 221, 255, 0.3)',
    // shadowGreen: 'rgba(30, 215, 96, 0.5)',
    shadowGreen: 'rgba(93, 204, 104, 0.4)',
    glowBlue: '#00ddff',
    glowGreen: '#1ed760',
  },
  card: {
    text: '#fff',
    background: 'rgb(66, 65, 77)'
  },
}
export const light = {
  text: '#181a1b',
  background: '#e8e6e3',
  colors: {
    success: '#038857',
    warn: '#ffd548',
    danger: 'rgb(255, 59, 48)',
    skyie: '#22bad9',
    white: '#e8e6e3',
    gray: '#d1d1d1',
    notification: 'rgb(255, 59, 48)',
    shadowBlue: 'rgba(34, 186, 217, 0.3)',
    shadowGreen: 'rgba(93, 204, 104, 0.4)',
    glowBlue: '#00ddff',
    glowGreen: '#1ed760',
  },
  card: {
    // text: '#8c8c8c',
    text: '#000',
    background: '#fefefe',
  },
}

export type Theme = typeof dark
