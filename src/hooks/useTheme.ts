import { useState } from 'react'
import { useColorScheme } from 'react-native'
import { dark, light, Theme } from 'styles'
import { THEMES } from 'appConstants'

const useTheme = () => {
  const scheme = useColorScheme()
  const [current, changeTheme] = useState(scheme)
  const themes: { dark: Theme, light: Theme } = {
    dark,
    light
  }
  const toggleTheme = () => {
    const nextTheme = current === THEMES.LIGHT ? THEMES.DARK : THEMES.LIGHT
    changeTheme(nextTheme)
  }
  return [themes[current], toggleTheme]
}

export default useTheme
