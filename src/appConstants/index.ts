export enum KEYS {
  DB_KEY = 'REALM_DB_KEY',
}

export enum THEMES {
  DARK = 'dark',
  LIGHT = 'light',
}

export enum Variants {
  warn = 'warn',
  skyie = 'skyie',
  danger = 'danger',
  success = 'success',
}

export type Routes = 'Home' | 'Tasks'

export const ICONS: { [key: string]: string } = {
  Home: 'home-outline',
  Tasks: 'ios-list-outline',
}

export const ActiveIcons: { [key: string]: string } = {
  Home: 'home-outline',
  Tasks: 'ios-list-outline',
}
