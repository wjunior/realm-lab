import * as React from 'react'
import { useTheme } from 'styled'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { Home, TaskList } from 'screens'
import { ActiveIcons, ICONS } from 'appConstants'

const Tab = createBottomTabNavigator()

function TabStack() {
  const { colors } = useTheme()

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName = focused ? ActiveIcons[route.name] : ICONS[route.name]
          return <Ionicons name={iconName} size={size} color={color} />
        },
        tabBarActiveTintColor: colors.skyie,
        tabBarInactiveTintColor: colors.gray,
      })}>
      <Tab.Screen name="Home" component={Home} />
      <Tab.Screen name="Tasks" component={TaskList} />
    </Tab.Navigator>
  )
}

export default TabStack
