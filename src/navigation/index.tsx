import { useColorScheme } from 'react-native'
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native'

import TabStack from './tab'

function MainNavigation(props: any) {
  const scheme = useColorScheme()
  return (
    <NavigationContainer theme={DarkTheme}>
    {/* <NavigationContainer theme={scheme === 'dark' ? DarkTheme : DefaultTheme}> */}
      <TabStack />
    </NavigationContainer>
  )
}

export default MainNavigation
