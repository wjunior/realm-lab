import { GestureHandlerRootView } from 'react-native-gesture-handler'
import Routes from 'navigation'
import { ThemeProvider } from 'styled'
import { useTheme } from 'hooks'

function Main(props: any) {
  const [theme, toggleTheme] = useTheme()
  return (
    <GestureHandlerRootView style={{ flex: 1 }}>
      <ThemeProvider theme={theme}>
        <Routes />
      </ThemeProvider>
    </GestureHandlerRootView>
  )
}
export default Main
