import styled, { css, VariantCommom } from 'styled'
import { TouchableOpacityProps, TextProps } from 'react-native'
import { Variants } from 'appConstants'

interface TouchableType extends TouchableOpacityProps, VariantCommom {}

interface LabelType extends TextProps, VariantCommom {} 

const labelColorVariant = ({ theme, variant }: LabelType) => {
  const color = theme.colors[variant] ?? theme.colors['skyie']
  return css`
    color: ${({ disable }: VariantCommom) => (disable ? '#CCC' : color)};
  `
}

export const Label = styled.Text<LabelType>`
  font-size: 18px;
  font-family: bold;
  text-align: center;
  text-transform: uppercase;
  ${labelColorVariant}
  ${({outline, theme}: VariantCommom) => !outline ? css`
    color: ${theme.text};
  `: '' }
`


const textColor = ({ theme }: VariantCommom) => theme.text

const outlined = ({ theme, variant, outline }: VariantCommom) => {
  const version = Variants[variant]
  const color = theme.colors[version]
  return outline
    ? css`
        border: 2px solid ${color};
        background-color: transparent;
      `
    : ''
}

const buttonColorVariant = ({ theme, variant }: VariantCommom) => {
  const color = theme.colors[variant] ?? theme.colors[Variants.skyie]
  return css`
    background-color: ${color};
    border: 2px solid ${color};
  `
}

export const Button = styled.TouchableOpacity<TouchableType>`
  height: 50px;
  min-width: 100px;
  border-radius: 4px;
  border-radius: 4px;
  align-items: center;
  flex-direction: row;
  margin-vertical: 5px;
  margin-horizontal: 2px;
  padding-horizontal: 5px;
  justify-content: center;
  padding-vertical: 0.25px;
${'' /* a sequencia abaixo é obrigatória para nao haver sobrescrição de estilos */}
  ${buttonColorVariant}
  ${outlined}
`
