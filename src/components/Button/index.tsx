import { Variants } from 'appConstants'
import * as S from './styles'

interface ButtonProps {
  label: string
  disable?: boolean
  outline?: boolean
  onPress: () => void
  variant?: Variants.danger | Variants.warn | Variants.success | Variants.skyie
}

const Button = ({
  label,
  variant,
  onPress,
  disable,
  ...props
}: ButtonProps) => {
  const press = disable ? () => {} : onPress
  return (
    <S.Button variant={variant} onPress={press} disable={disable} {...props}>
      <S.Label variant={variant} disable={disable} {...props}>
        {label}
      </S.Label>
    </S.Button>
  )
}

export default Button
