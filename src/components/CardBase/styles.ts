import styled from 'styled';

export const Card = styled.View`
  width: 100%;
  padding: 5px;
  elevation: 3;
  border-radius: 10px;
  margin-vertical: 2%;
  background-color: #fff;
`
