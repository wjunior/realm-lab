import React, { ReactNode } from 'react'
import * as S from './styles'

interface CardBaseProps {
  children?: ReactNode
}
const CardBase = ({ children }: CardBaseProps) => (
  <S.Card>{children}</S.Card>
)

export default CardBase
