import styled from 'styled-components/native'
import { TextProps } from 'react-native'

interface LabelProps extends TextProps {
  isActive: boolean
}

export const Container = styled.View`
  height: 50px;
  margin: 15px 0px;
  position: relative;
`

export const Label = styled.Text<LabelProps>`
  position: absolute;
  bottom: ${({ isActive }) => (isActive ? '45px' : '17px')};
  font-weight: ${({ isActive }) => (isActive ? 700 : 500)};
`

export const TextInput = styled.TextInput`
  color: #fff;
  height: 50px;
  font-size: 14px;
  border-bottom-width: 0.5px;
  border-bottom-color: #22bed9;
`
// color: #24272b;
