import React, { useState } from 'react'
import { TextInputProps } from 'react-native'
import * as S from './styles'

const Input: React.FC<TextInputProps> = ({
  placeholder,
  value,
  ...props
}: TextInputProps) => {
  const [isFocused, setFocus] = useState(false)
  const isActive: boolean = value !== ''
  return (
    <S.Container>
      <S.Label isActive={isFocused || isActive}>{placeholder}</S.Label>
      <S.TextInput
        {...props}
        onFocus={() => {
          setFocus(true)
        }}
        onBlur={() => {
          setFocus(false)
        }}
      />
    </S.Container>
  )
}
export default Input
