import React, { ReactNode } from 'react'
import Swipeable from 'react-native-gesture-handler/Swipeable'
import * as S from './styles'

interface CardSwipeableProps {
  children?: ReactNode
}
const CardSwipeable = ({ children }: CardSwipeableProps) => {
  const LeftSwipeActions = () => {
    return (
      <S.ActionsBox>
        <S.ActionButtom variant="skyie">
          <S.ActionButtomIcon>Pausar</S.ActionButtomIcon>
        </S.ActionButtom>
      </S.ActionsBox>
    )
  }
  const rightSwipeActions = () => {
    return (
      <S.ActionsBox>
        <S.ActionButtom variant="success">
          <S.ActionButtomIcon>editar</S.ActionButtomIcon>
        </S.ActionButtom>
        <S.ActionButtom variant="danger" round>
          <S.ActionButtomIcon>X</S.ActionButtomIcon>
        </S.ActionButtom>
      </S.ActionsBox>
    )
  }
  const swipeFromRightOpen = () => {
    // alert('Swipe from right')
  }
  const swipeFromLeftOpen = () => {
    // alert('Swipe from left')
  }
  return (
    <S.CardContainer>
      <Swipeable
        renderLeftActions={LeftSwipeActions}
        renderRightActions={rightSwipeActions}
        onSwipeableRightOpen={swipeFromRightOpen}
        onSwipeableLeftOpen={swipeFromLeftOpen}>
        <S.Card>
          {/* <S.Title>Something i'm doing...</S.Title> */}
          <S.Title>{children}</S.Title>
          {/* <S.CardCounterOverlay>{children}</S.CardCounterOverlay> */}
          <S.CardCounterOverlay />
        </S.Card>
      </Swipeable>
    </S.CardContainer>
  )
}

export default CardSwipeable
