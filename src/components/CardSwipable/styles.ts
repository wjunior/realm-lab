// import { CardBase } from 'components'
import styled, { css, VariantCommom } from 'styled'

export const CardContainer = styled.View`
  width: 100%;
  elevation: 3;
  min-height: 90px;
  max-height: 90px;
  margin-vertical: 3%;
  flex-direction: row;
  border-radius: 10px;
  justify-content: space-between;
  background-color: ${({ theme }) => theme.card.background};
`
export const Card = styled.View`
  width: 100%;
  border-radius: 10px;
  ${'' /* background-color: #313b3e; */}
  ${'' /* background-color: rgb(66, 65, 77); */}
  min-height: 90px;
  max-height: 90px;
  flex-direction: row;
  justify-content: space-between;
  background-color: ${({ theme }) => theme.card.background};
`
export const CardCounterOverlay = styled.View`
  width: 97%;
  height: 100%;
  position: absolute;
  ${'' /* border-radius: 10px; */}
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
  background-color: ${({ theme }) => theme.colors.shadowBlue};
`
export const Title = styled.Text`
  width: 100%;
  margin-top: 5px;
  margin-left: 5px;
  font-size: 18px;
  font-weight: bold;
  color: ${({ theme }) => theme.card.text};
`
export const Description = styled.Text`
  color: ${({ theme }) => theme.card.text};
  margin-top: 5px;
  margin-left: 5px;
  font-size: 18px;
  font-weight: 400;
`

const variantColorHelper = ({ theme, variant }: VariantCommom) => {
  const color = theme.colors[variant] ?? theme.colors['success']
  return css`
    background-color: ${color};
  `
}

const actionsRoundHelper = ({ theme, round }: VariantCommom) => {
  return round
    ? css`
        border-top-right-radius: 10px;
        border-bottom-right-radius: 10px;
      `
    : ''
}

export const ActionsBox = styled.View`
  width: 50%;
  flex-direction: row;
`

export const ActionButtom = styled.View`
  width: 50%;
  height: 100%;
  align-items: center;
  padding-horizontal: 5px;
  justify-content: center;
  ${variantColorHelper}
  ${actionsRoundHelper}
`

export const ActionButtomIcon = styled.Text`
  font-size: 18px;
  font-weight: 500;
  text-align: center;
  color: ${({ theme }) => theme.card.text};
`

// export const Card = styled(CardBase)`
//   min-height: 80px;
//   background-color: #22bed9;
//   background-color: rgb(66, 65, 77);
// `
