import { Variants } from 'appConstants';
import * as styledComponents from 'styled-components/native'

import { Theme as ThemeInterface} from '../styles'

const {
  default: styled,
  css,
  ThemeProvider,
  useTheme,
} = styledComponents as styledComponents.ReactNativeThemedStyledComponentsModule<ThemeInterface>;

export interface VariantCommom {
  disable: boolean
  outline: boolean
  variant: Variants
  theme: ThemeInterface
}

export { css, ThemeProvider, useTheme };
export default styled;
