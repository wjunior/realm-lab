import { BSON } from 'realm'
import db, { IDB } from 'db'
import { ITaskInput, ITaskSchema, TaskFields } from 'db/types'
import { makeRealmObj } from './realm'

// generators process
async function* processCreateTodo(params: ITaskInput) {
  try {
    const Store: IDB = await db()
    const _id = new BSON.ObjectId()
    const owner = new BSON.ObjectId()
    const data = makeRealmObj({ _id, owner, ...params })

    yield Store.Task.create(data)
  } catch (error: any) {
    console.log('[ERROR] generator create todo error ====>', error.message)
    return {} as ITaskSchema
  }
}

async function* processListToDos() {
  try {
    const Store: IDB = await db()
    // Store.Task.clear()
    console.log('[LOG] <==== processListToDos ====>')
    yield Store.Task.findAll()
  } catch (error: any) {
    console.log('[ERROR] generator processListTodos error ====>', error.message)
    return [] as ITaskSchema[]
  }
}

async function* processUpdateTodo(id: BSON.ObjectId, params: ITaskInput) {
  try {
    const Store: IDB = await db()
    yield Store.Task.update(id, params)
  } catch (error: any) {
    console.log('[ERROR] generator create todo error ====>', error.message)
  }
}

async function* processDeleteTodo(id: BSON.ObjectId) {
  try {
    const Store: IDB = await db()
    yield Store.Task.delete(id)
  } catch (error: any) {
    console.log('[ERROR] generator create todo error ====>', error.message)
  }
}

async function* processToDoById(id: BSON.ObjectId) {
  try {
    const Store: IDB = await db()
    console.log('[LOG] <==== processToDoById ====>')
    yield Store.Task.findById(id)
  } catch (error: any) {
    console.log('[ERROR] generator processListTodos error ====>', error.message)
    return [] as ITaskSchema[]
  }
}

// CRUD functions
export async function createTodo(params: ITaskInput) {
  try {
    for await (const result of processCreateTodo(params)) {
      console.log('[LOG] <==== createTodo function ====>', result)
    }
  } catch (error: any) {
    console.log('[ERROR] function create toDo error ====>', error.message)
    throw new Error(error.message)
  }
}
// type Results = Realm.Results<Realm.Object<ITaskSchema, never>>

export async function listTodos(): Promise<ITaskSchema[]> {
  try {
    let items: unknown
    for await (const result of processListToDos()) {
      if (result) {
        items = result
      }
    }
    return items as ITaskSchema[]
  } catch (error: any) {
    console.log('[ERROR] function list todos error ====>', error.message)
    return [] as ITaskSchema[]
  }
}

export async function updateTodo(id: BSON.ObjectId, params: ITaskInput) {
  try {
    for await (const result of processUpdateTodo(id, params)) {
      console.log('[LOG] update result ====>', result)
    }
  } catch (error: any) {
    console.log('[ERROR] function update todo error ====>', error.message)
    throw new Error(error.message)
  }
}

export async function deleteTodo(id: BSON.ObjectId) {
  try {
    for await (const result of processDeleteTodo(id)) {
      console.log('[LOG] delete result ====>', result)
    }
  } catch (error: any) {
    console.log('[ERROR] function delete todo error ====>', error.message)
    throw new Error(error.message)
  }
}

export async function getToDoById(id: BSON.ObjectId): Promise<ITaskSchema> {
  try {
    let item: unknown
    for await (const result of processToDoById(id)) {
      if (result) {
        item = result
      }
    }
    return item as ITaskSchema
  } catch (error: any) {
    console.log('[ERROR] function list todos error ====>', error.message)
    return {} as ITaskSchema
  }
}
