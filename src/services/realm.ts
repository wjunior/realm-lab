
const RealmObjComplement = {
        keys: function (): string[] {
        throw new Error('Function not implemented.')
      },
      entries: function (): [string, any][] {
        throw new Error('Function not implemented.')
      },
      toJSON: function (): Record<string, unknown> {
        throw new Error('Function not implemented.')
      },
      isValid: function (): boolean {
        throw new Error('Function not implemented.')
      },
      objectSchema: function (): Realm.ObjectSchema {
        throw new Error('Function not implemented.')
      },
      linkingObjects: function <T>(objectType: string, property: string): Realm.Results<T & Realm.Object<unknown, never>> {
        throw new Error('Function not implemented.')
      },
      linkingObjectsCount: function (): number {
        throw new Error('Function not implemented.')
      },
      _objectKey: function (): string {
        throw new Error('Function not implemented.')
      },
      addListener: function (callback: Realm.ObjectChangeCallback<ITaskSchema>): void {
        throw new Error('Function not implemented.')
      },
      removeListener: function (callback: Realm.ObjectChangeCallback<ITaskSchema>): void {
        throw new Error('Function not implemented.')
      },
      removeAllListeners: function (): void {
        throw new Error('Function not implemented.')
      },
      getPropertyType: function (propertyName: string): string {
        throw new Error('Function not implemented.')
      }
}

export const makeRealmObj = <T>(params: T) => ({
  ...params,
  ...RealmObjComplement,
})