import EncryptedStorage from 'react-native-encrypted-storage'
import { KEYS } from 'appConstants'

const Security = {
  save: async (key: string, data: any) => {
    try {
      await EncryptedStorage.setItem(key, JSON.stringify(data))
    } catch (error: any) {
      // There was an error on the native side
      console.log('error when save', error.message)
    }
  },
  getInfo: async (key: string) => {
    try {
      const info = await EncryptedStorage.getItem(key)
      if (info) {
        return JSON.parse(info)
      }
      return false
    } catch (error: any) {
      // There was an error on the native side
      console.log('error when retrieve  someting', error.message)
    }
  },
  removeInfo: async (key: string) => {
    try {
      await EncryptedStorage.removeItem(key)
    } catch (error: any) {
      console.log('error when retrieve  someting', error.message)
    }
  },
  clear: async () => {
    try {
      await EncryptedStorage.clear()
    } catch (error: any) {
      console.log('error when retrieve  someting', error.message)
    }
  },
  genCryptKey: async () => {
    const hasKey = await Security.getInfo(KEYS.DB_KEY)

    if (hasKey) {
      const bytes: number[] = Object.values(hasKey)
      return new Int8Array(bytes)
    }
    if (!hasKey) {
      const bytes = new Int8Array(64)
      const key64 = Array.from(bytes, (x) => Math.floor(Math.random() * 10))
      const finalKey = new Int8Array(key64)
      Security.save(KEYS.DB_KEY, finalKey)
      return finalKey
    }
  },
  checkKey: async (key: string) => {
    try {
      const info = await EncryptedStorage.getItem(key)
      return !!info
    } catch (error: any) {
      // There was an error on the native side
      console.log('error when checking something', error.message)
      return false
    }
  },
}

export default Security
