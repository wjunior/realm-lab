import * as S from './styles'
import { Button, Input } from 'components'
import { useState } from 'react'
import { TodoService } from 'services'
import { ToastAndroid, ActionSheetIOS, Platform } from 'react-native'
import { Variants } from 'appConstants'

const Home = () => {
  const [todo, setTodo] = useState('')
  const saveSuccess = Platform.select({
    ios: () => {
      ActionSheetIOS.showActionSheetWithOptions(
        {
          options: [],
          title: 'Salvo',
          userInterfaceStyle: 'light',
        },
        () => null,
      )

      setTimeout(() => {
        ActionSheetIOS?.dismissActionSheet()
      }, 1000)
    },
    android: () => {
      ToastAndroid.showWithGravityAndOffset('Salvo', 5, 2, 2, 2)
    },
  })
  const saveTodo = async () => {
    const dueDate = new Date(new Date().setHours(new Date().getHours() + 2))
    try {
      await TodoService.createTodo({
        title: todo,
        dueDate,
        priority: 1,
        status: 'to do',
      })
      // setTodo('')
      saveSuccess?.()
    } catch (error: any) {
      console.log('error create todo component layer =>', error.message)
    }
  }
  return (
    <S.Container>
      <S.Title>What i'm doing or will do...</S.Title>
      <S.FormContainer>
        <Input
          value={todo}
          placeholder="What to do now?"
          onChangeText={(txt) => setTodo(txt)}
        />
        <Button label="Salvar" onPress={saveTodo} variant={Variants.skyie} outline />
      </S.FormContainer>
    </S.Container>
  )
}
export default Home
