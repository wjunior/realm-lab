import styled from 'styled'

export const Container = styled.View`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-vertical: 5%;
`
// background-color: #22bed9;
export const FormContainer = styled.View`
  flex: 1;
  padding-vertical: 10%;
  width: 80%;
`

export const Title = styled.Text`
  font-size: 20px;
  text-align: center;
`
