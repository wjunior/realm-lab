import * as S from './styles'
import { useCallback, useEffect, useState } from 'react'
import { TodoService } from 'services'
import { ITaskSchema } from 'db/types'
import { Button, CardSwipable } from 'components'
import { Variants } from 'appConstants'
import { View } from 'react-native'
import { BSON } from 'realm'

const TaskList = () => {
  const [tasks, setTasks] = useState<ITaskSchema[]>([])

  const loadTasks = useCallback(async () => {
    const todos: ITaskSchema[] = await TodoService.listTodos()
    setTasks(() => [...todos])
  }, [])

  const deleteTask = useCallback(async (id: BSON.ObjectId) => {
    try {
      await TodoService.deleteTodo(id)
      loadTasks()
    } catch (error: any) {
      console.log('erro ao deletar====>', error.message)
    }
  }, [])

  useEffect(() => {
    loadTasks()
  }, [])

  const renderItem = ({ item }: { item: ITaskSchema }) => {
    const { _id, title, priority, status } = item
    return (
      <CardSwipable key={_id}>
        <S.ListItemTitle> {title} </S.ListItemTitle>
        <S.ListItemInfo> prioridade: {priority} </S.ListItemInfo>
        <S.ListItemInfo> status: {status} </S.ListItemInfo>
      </CardSwipable>
    )
  }

  return (
    <S.Container>
      <S.Title>What i'm doing...</S.Title>
      <S.List
        data={tasks}
        renderItem={renderItem}
        keyExtractor={(item: ITaskSchema) => `${item._id}`}
        ListFooterComponent={<View style={{ height: 50 }} />}
      />
    </S.Container>
  )
}

export default TaskList
