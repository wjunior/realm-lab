import styled from 'styled'
import { FlatListProps } from 'react-native'
import { ITaskSchema } from 'db/types'

export const Container = styled.View`
  flex: 1;
  padding-vertical: 5%;
  padding-horizontal: 5%;
  ${'' /* background-color: #22bed9; */}
`

export const List = styled.FlatList<FlatListProps<ITaskSchema>>`
  padding-vertical: 10%;
`

export const ListItem = styled.View`
  width: 100%;
  padding: 5px;
  elevation: 3;
  border-radius: 10px;
  margin-vertical: 2%;
  ${'' /* background-color: #fff; */}
  ${'' /* background-color: #313b3e; */}
  background-color: rgb(66, 65, 77);
`
// flex-direction: row;

export const ListItemTitle = styled.Text`
  ${'' /* color: #335599; */}
  color: #fff;
  font-size: 18px;
  font-weight: bold;
  margin-vertical: 2px;
  margin-horizontal: 2px;
`
export const ListItemInfo = styled.Text`
  ${'' /* color: #555599; */}
  color: #fff;
  font-size: 17px;
  margin-vertical: 2px;
  margin-horizontal: 2px;
`

export const Title = styled.Text`
  font-size: 20px;
  text-align: center;
  margin-vertical: 10px;
`
